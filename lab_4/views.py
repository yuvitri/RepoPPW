from django.shortcuts import render
from lab_2.views import landing_page_content
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message


    # Create your views here.
response = {'author': "Yuvitri"} #TODO Implement yourname
about_me = ['Bismillah berhasil pindah dari Fasilkom', 'setiap hari senin malam sedih knp sda susah ya','Semoga Masuk FKUI 2018','Mohon doanya untuk lulus semua matkul smt 3', 'Biar bisa cuti smt 4', 'Pengen jadi dokter Neurologi']

def index(request):
    html = 'lab_4/lab_4.html'
    response['content'] = landing_page_content
    response['about_me'] = about_me
    response['message_form'] = Message_Form
    return render(request, html, response)

def message_post(request):
    if request.method == 'POST':
        form = Message_Form(request.POST or None)
        if form.is_valid():
            response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
            response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
            response['message'] = request.POST['message']
            message = Message(name=response['name'], email=response['email'],
                              message=response['message'])
            message.save()
            html = 'lab_4/form_result.html'
            return render(request, html, response)
        else:
            response['form'] = form
            html = 'lab_4/lab_4.html'
            return render(request, html, response)
    else:
        return HttpResponseRedirect('/lab-4/')


def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'lab_4/table.html'
    return render(request, html , response)
