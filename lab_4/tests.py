from django.test import TestCase, Client
from django.http import HttpRequest
from django.urls import resolve
from django.utils import timezone
from lab_1.views import mhs_name
from .views import index, about_me, landing_page_content
from .models import Message
from .forms import Message_Form

class Lab4UnitTest(TestCase):
    def test_lab_4_url_is_exist(self):
        response = Client().get('/lab-4/')
        self.assertEqual(response.status_code, 200)

    def test_about_me_more_than_6(self):
        self.assertTrue(len(about_me) >= 6)

    def test_lab4_using_index_func(self):
        found = resolve('/lab-4/')
        self.assertEqual(found.func, index)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')

        #Checking whether have Bio content
        self.assertIn(landing_page_content, html_response)

        #Chceking whether all About Me Item is rendered
        for item in about_me:
            self.assertIn(item,html_response)

    def test_model_can_create_new_message_and_has_str_function(self):
        #Creating a new activity
        new_activity = Message.objects.create(name=mhs_name,email='test@gmail.com',message='This is a test')

        #Retrieving all available activity
        counting_all_available_message= Message.objects.all().count()
        self.assertEqual(counting_all_available_message,1)

        #Testing str function
        self.assertEqual(str(new_activity),new_activity.message)

    def test_root_url_now_is_using_index_page_from_lab_4(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 301)
        self.assertRedirects(response,'/lab-4/',301,200)


    def test_form_message_input_has_placeholder_and_css_classes(self):
        form = Message_Form()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('<label for="id_name">Nama:</label>', form.as_p())
        self.assertIn('<label for="id_email">Email:</label>', form.as_p())
        self.assertIn('<label for="id_message">Message:</label>', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Message_Form(data={'name': '', 'email': '', 'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_can_save_a_POST_request(self):
        response = self.client.post('/lab-4/add_message/', data={'name': 'Satraul', 'email': 'satraul@icloud.com', 'message': 'Kafka on the shore'})
        counting_all_available_activity = Message.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

        self.assertEqual(response.status_code, 200)

        new_response = self.client.get('/lab-4/result_table')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Kafka on the shore', html_response)

    def test_reject_invalid_POST_request(self):
        response = self.client.post('/lab-4/add_message/', data={'name': 'Satraul', 'email': 'satraul_at_icloud.com', 'message': 'Kafka on the shore'})
        counting_all_available_activity = Message.objects.all().count()
        self.assertEqual(counting_all_available_activity, 0)

        new_response = self.client.get('/lab-4/result_table')
        html_response = new_response.content.decode('utf8')
        self.assertNotIn('Kafka on the shore', html_response)

    def test_redirect_GET_request(self):
        response = self.client.get('/lab-4/add_message/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url,'/lab-4/')

    def test_lab_4_using_navbar_template(self):
        response = Client().get('/lab-4/')
        self.assertTemplateUsed(response, 'lab_4/partials/header.html')

    def test_lab_4_using_copyright_template(self):
        response = Client().get('/lab-4/')
        self.assertTemplateUsed(response, 'lab_4/partials/footer.html')