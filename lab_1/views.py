from django.shortcuts import render
from datetime import datetime, date


# Enter your name here
mhs_name = 'yuvitri annisa' # TODO Implement this
birth_date = date(1998,8,14)
birth_year = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(birth_year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
	thisYear = datetime.now().year
	return thisYear- birth_year
